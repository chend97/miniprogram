/**   引入请求函数    */
import {request} from '../../network/request.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    goods: [],
    isShow: false,
    inputValue: '',
    timer: null
  },
  handleInput(e){
    let value = e.detail.value
    // 判断输入的值是否符合条件
    if(!value.trim()){
      clearTimeout(this.data.timer);
      this.setData({
        isShow: false,
        goods: []
      })
      return
    }
    this.setData({
      isShow: true
    })
    // 防抖
    clearTimeout(this.data.timer);
    this.data.timer = setTimeout( () => {
      request({url: '/goods/qsearch',data: {query:value}}).then( res => {
        console.log(res);
        this.setData({
          goods: res.data.message
        })
      })
    },1000)
  },
  // 取消重置事件
  handleBtnTap(){
    this.setData({
      isShow: false,
      goods: [],
      inputValue: ''
    })
  }
})