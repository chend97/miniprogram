/**   引入请求函数    */
import {request} from '../../network/request.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      { 
        id: 0,
        title: '综合'
      },
      {
        id: 1,
        title: '销量'
      },
      {
        id: 2,
        title: '价格'
      }
    ],
    goodsList: [],
    cid: 0,
    // 请求数据条数
    total: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.reqConfig.cid = options.cid || '';
    this.reqConfig.query = options.query || '';

  },
  // 请求的参数
  reqConfig:{
    query: '',
    cid: "",
    pagenum: 1,
    pagesize: 10,
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    this.getGoodsList()
  },
  getGoodsList(){
    console.log(this.reqConfig);
    request({
      url: '/goods/search',
      data: this.reqConfig
    }).then( res => {
      console.log(res);
      this.setData({
        goodsList: [...this.data.goodsList,...res.data.message.goods],
        total: res.data.message.total
      })
    })
  },
  // 上拉加载更多事件
  onReachBottom(){
    let page = Math.ceil(this.data.total/this.reqConfig.pagesize)
    // 还有下一页
    if(this.reqConfig.pagenum < page){
      this.reqConfig.pagenum++;
      this.getGoodsList()
    } else {
      // 提示没有下一页数据了
    }
  }
})