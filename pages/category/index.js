
/**   引入请求函数    */
import {request} from '../../network/request.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    categoryData: [],
    categoryCnt: [],
    count: 0,
    // 显示在哪个位置
    scrollTop: 0
  },
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 发送请求，设置缓存数据
    // 获取本地存储数据
    let cates = wx.getStorageSync("localCates");  
    if(!cates.data) {
      // 没有数据就发起请求
      console.log(cates);
      this.getCategoryData()
    } else {
      if(Date.now()- cates.time > 1000*60*5){
        this.getCategoryData()
      } else {
        this.setData({
          categoryData: cates.data,
          categoryCnt : cates.data[this.data.count].children
        })
      }
    }

  },
  getCategoryData(){
    request({
      url: "/categories"
    }).then( res => {
      // 存储数据
      wx.setStorageSync("localCates", {time: Date.now(),data: res.data.message});
      this.setData({
        categoryData: res.data.message,
        categoryCnt : res.data.message[this.data.count].children
      })
    })
  },
  asideTap(event){
    let index = event.currentTarget.dataset.index;
    this.setData({
      count: index,
      scrollTop: 0
    })
    this.getCategoryData();
  }
})