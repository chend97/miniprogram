// pages/auth/index.js
import {request} from '../../network/request.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {

  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {

  },
  onShow: function () {

  },
  handleGetUserIfo(e){
    console.log(e);
    // 获取用户信息
    let code = '';
    const {encryptedData,rawData,iv,signature} = e.detail;
    // 获取code
    wx.login({
      timeout:10000,
      success: (result) => {
        code = result.code;
        const loginConfig = {encryptedData,rawData,iv,signature,code}
        console.log(loginConfig);
        // 获取token
        request({url: '/users/wxlogin', data: {loginConfig}, method: 'POST'}).then( res => {
          let token = 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJ1aWQiOjIzLCJpYXQiOjE1NjQ3MzAwNzksImV4cCI6MTAwMTU2NDczMDA3OH0.YPt-XeLnjV-_1ITaXGY2FhxmCe4NvXuRnRB8OMCfnPo';
          wx.setStorageSync('token',token);
          wx.navigateBack({delta: 1})
        }).catch( err => {
          console.log(err);
        })
      }
    });   
  }
})