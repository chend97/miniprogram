// pages/fendback/index.js
Page({

  /**
   * 页面的初始数据
   */
  data: {
    tabs: [
      { 
        id: 0,
        title: '体验问题'
      },
      {
        id: 1,
        title: '商品,商家投诉'
      }
    ],
    imgList: []
  },
  btnTap(){
    wx.chooseImage({
      count: 9,
      sizeType: ['original', 'compressed'],
      sourceType: ['album', 'camera'],
      success: (result) => {
        this.setData({
          imgList: [...this.data.imgList,...result.tempFilePaths]
        })
      }
    }); 
  },
  clearTap(e){
    let src = e.currentTarget.dataset.src;
    let idx = this.data.imgList.findIndex(item => item == src)
    let imgList = this.data.imgList
    imgList.splice(idx,1);
    this.setData({
      imgList
    })
  }
})