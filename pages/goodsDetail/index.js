import {request} from '../../network/request.js';
import {Goods} from '../../network/details.js';
Page({

  /**
   * 页面的初始数据
   */
  data: {
    // 商品id
    goodsId: 0,
    // 商品banner
    goodsBanner: [],
    goodsMsg: {}
  },

  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    this.setData({
      goodsId: Number(options.goodsId)
    })
  },
  /**
   * 生命周期函数--监听页面显示 
   */
  onShow: function () {
    this.getGoodsDetail()
  },
  getGoodsDetail(){
    request({
      url: '/goods/detail',
      data: {goods_id: this.data.goodsId }
    }).then(res => {
      console.log(res);
      let goodsMsg = new Goods(
        res.data.message.goods_price,
        res.data.message.goods_name,
        res.data.message.goods_introduce,
        res.data.message.goods_small_logo,
        res.data.message.goods_id
      )
      this.setData({
        goodsBanner: res.data.message.pics,
        goodsMsg
      })
    })
  },
  // 轮播图点击事件
  imgTap(e){
    let urls = this.data.goodsBanner.map( v=> v.pics_mid);
    console.log(e);
    let current = e.currentTarget.dataset.url
    wx.previewImage({
      current, // 当前显示图片的http链接
      urls // 需要预览的图片http链接列表
    })
  },
  // 购物车添加事件
  addCartTab(){
    // console.log('购物车添加事件');
    // 获取购物车数据
    let cartList = wx.getStorageSync('cartList') || [];
    // 获取当前商品在购物车列表中的下标
    let index = cartList.findIndex( item => item.goods_id === this.data.goodsId);
    if(index === -1){
      // 商品不存在
      this.data.goodsMsg.num = 1;
      this.data.goodsMsg.checkType = true
      cartList.push(this.data.goodsMsg)
    } else {
      // 商品数量加1
      cartList[index].num++;
    }
    wx.setStorageSync('cartList',cartList);
    // 提示信息
    wx.showToast({
      title: '添加成功',
      icon: 'success',
      // 防止手抖，1.5秒内只能点击一次
      mask: true
    })
  }
})