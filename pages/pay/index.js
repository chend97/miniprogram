
import {request} from '../../network/request.js';
import {showToast} from '../../lib/until.js';
Page({
  /**
   * 页面的初始数据
   */
  data: {
    addressData: [],
    cartList: [],
    cartSum: 0,
    cartNum: 0
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
  },
  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    // 获取购物地址
    const addressData = wx.getStorageSync('address')||[];
    // 获取购物车中需要购买的商品数据
    const cartList1 = wx.getStorageSync('cartList')||[];
    const cartList = cartList1.filter( item => item.checkType)
    // 获取支付价格
    const cartSum = wx.getStorageSync('cartSum') || 0;
    // 获取支付的商品数量
    const cartNum = wx.getStorageSync('cartNum') || 0;
    this.setData({
      addressData,
      cartList,
      cartSum,
      cartNum
    })
  },
  handeBuy(){
    // console.log('支付');
    // 1. 判断用户token是否存在，不存在则跳转页面获取获取用户权限
    const token = wx.getStorageSync('token') || '';
    if(!token){
      // 不存在token
      wx.navigateTo({url: '/pages/auth/index'});
    } else {
      // 1.创建订单号
      const header = {Authorization: token};
      const order_price = this.data.cartSum;
      const consignee_addr = this.data.addressData;
      let goods = []
      this.data.cartList.forEach( item => {
        goods.push({
          goods_id: item.goods_id,
          goods_number: item.num,
          goods_price: item.goods_price
        })
      })
      // 准备好了参数开始发送请求
      request({
        url: '/my/orders/create',
        data: {order_price,consignee_addr,goods},
        method: 'POST',
        header
      }).then( res => {
        showToast({title: res.data.meta.msg})
      })
    }
  }
})