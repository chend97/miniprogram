// pages/cart/index.js
import initComputed from 'wx-computed'
import {showModal,showToast} from '../../lib/until.js'
Page({
  /**
   * 页面的初始数据
   */
  data: {
    addressData: [],
    cartList: []
  },
  /**
   * 生命周期函数--监听页面加载
   */
  onLoad: function (options) {
    initComputed(this)
  },
  
  /**
   * 生命周期函数--监听页面显示
   */
  onShow: function () {
    const addressData = wx.getStorageSync('address')||[];
    const cartList = wx.getStorageSync('cartList')||[];
    this.setData({
      addressData,
      cartList,
      isSelectAll: false
    })
  },
  selectAddress(){
    wx.chooseAddress({
      success: (result) => {
        wx.setStorageSync('address', result);
        this.setData({
          addressData: result
        })
      }
    });
      
  },
  computed: {
    cartSum(){
      let sum = 0;
      this.data.cartList.forEach( item => {
        if(item.checkType){
          sum+=(item.price*item.num)
        }
      });
      return sum
    },
    cartLength(){
      let len = 0;
      this.data.cartList.forEach( item => {
        if(item.checkType){
          len+=item.num
        }
      });
      return len;
    },
    selectAll(){
      if(this.data.cartList.length>0){
        return this.data.cartList.every(item => item.checkType)
      } else {
        return false
      }
    }
  },
  // 商品选中事件
  handeChange(e){
    let goods_id = e.currentTarget.dataset.id
    let cartList = this.data.cartList;
    let idx = cartList.findIndex( item => item.goods_id === goods_id);
    cartList[idx].checkType = !cartList[idx].checkType;
    this.setData({
      cartList
    })
    // 改变缓存中的购物车数据
    wx.setStorageSync('cartList',cartList)
  },
  // 全选按钮点击事件
  handeSelectAll(e){
    let type = e.currentTarget.dataset.type;
    let cartList = this.data.cartList;
    cartList.forEach( item => {
      item.checkType = !type
    });
    this.setData({
      cartList
    })
    // 改变缓存中的购物车数据
    wx.setStorageSync('cartList',cartList)
  },
  // 商品数量操作
  goodsSub(e){
    let goods_id = e.currentTarget.dataset.goodsid;
    let cartList = this.data.cartList;
    let idx = cartList.findIndex( item => item.goods_id === goods_id);
    if(cartList[idx].num > 1){
      cartList[idx].num--;
    } else {
      let that = this;
      // wx.showModal({
      //   title: '提示',
      //   content: '是否需要删除商品',
      //   success (res) {
      //     if (res.confirm) {
      //       // console.log('用户点击确定')
      //       cartList.splice(idx,1);
      //       that.setData({
      //         cartList
      //       })
      //       // 改变缓存中的购物车数据
      //       wx.setStorageSync('cartList',cartList)
      //     } else if (res.cancel) {
      //       // console.log('用户点击取消')
      //       return;
      //     }
      //   }
      // })
      showModal({content: '是否需要删除商品'}).then( () => {
        cartList.splice(idx,1);
        that.setData({
          cartList
        })
        // 改变缓存中的购物车数据
        wx.setStorageSync('cartList',cartList)
      }).catch( () => {
        return;
      })
    }
    this.setData({
      cartList
    })
    // 改变缓存中的购物车数据
    wx.setStorageSync('cartList',cartList)
  },
  goodsAdd(e){
    let goods_id = e.currentTarget.dataset.goodsid;
    let cartList = this.data.cartList;
    let idx = cartList.findIndex( item => item.goods_id === goods_id);
    cartList[idx].num++;
    this.setData({
      cartList
    })
    // 改变缓存中的购物车数据
    wx.setStorageSync('cartList',cartList)
  },
  // 商品结算事件
  handeSettlement(e){
    // 判断是否有商品
    let res = true;
    this.data.cartList.forEach( item => {
      if(item.checkType){
        res = false
      }
    });
    if(res){
      showToast({title: '还未选择结算商品！'});
      return;
    }
    // 判断是否添加地址
    if(this.data.addressData.length <= 0){
      showToast({title: '还未选择收货地址！'});
      return;
    }
    console.log(e);
    // 存储总价格
    wx.setStorageSync('cartSum',e.currentTarget.dataset.cartsum)
    // 存储数量
    wx.setStorageSync('cartNum',e.currentTarget.dataset.cartlength)
    // 跳转结算页面
    wx.navigateTo({
      url: '/pages/pay/index'
    });
  }
})