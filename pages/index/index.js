
/**   引入请求函数    */
import {request} from '../../network/request.js';
Page({
  data: {
    swiperList: [],
    navList: [],
    floorData: []
  },
  onShow(){
    this.getSwiperData();
    this.getNavData();
    this.getFloorData()
  },
  // 请求轮播图数据
  getSwiperData(){
    request({
      url: "/home/swiperdata"
    }).then( res => {
      this.setData({
        swiperList: res.data.message
      })
    })
  },
  // 请求导航数据
  getNavData(){
    request({
      url: "/home/catitems"
    }).then( res => {
      this.setData({
        navList: res.data.message
      })
    })
  },
  // 请求楼层数据
  getFloorData(){
    request({
      url: "/home/floordata"
    }).then( res => {
      console.log(res);
      this.setData({
        floorData: res.data.message
      })
    })
  }
})