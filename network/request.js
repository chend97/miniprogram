export function request(config){
  const baseUrl = "https://api-hmugo-web.itheima.net/api/public/v1";
  // 发送请求之前弹出加载中提示
  wx.showLoading({
    title: '加载中',
    mask: true,
  });
  return new Promise( (resolve,reject) => {
    wx.request({
      ...config,
      url: baseUrl + config.url,
      success(result){
        resolve(result)
      },
      fail(err){
        reject(err)
      },
      complete(){
        // 关闭提示
        wx.hideLoading();
      }
    })
  })
}


