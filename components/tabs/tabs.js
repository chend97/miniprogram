// components/tabs/tabs.js
Component({
  /**
   * 组件的属性列表
   */
  properties: {
    tabs:{
      type: Array,
      value: []
    }
  },

  /**
   * 组件的初始数据
   */
  data: {
    countIdx: 0
  },

  /**
   * 组件的方法列表
   */
  methods: {
    tabItemTap(e){
      let idx = e.currentTarget.dataset.idx
      this.setData({
        countIdx: idx
      })
    }
  }
})
