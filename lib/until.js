// 显示模态对话框
export function showModal({content}){
  return new Promise( (reslove,reject) => {
    wx.showModal({
      title: '提示',
      content: content,
      success (res) {
        if (res.confirm) {
          reslove()
        } else if (res.cancel) {
          reject()
        }
      }
    })
  })
}
// 显示消息提示框
export function showToast({title}){
  return new Promise( (reslove,reject) => {
    wx.showToast({
      title: title,
      icon: 'none',
      success (res) {
        if (res.confirm) {
          reslove()
        } else if (res.cancel) {
          reject()
        }
      }
    })
  })
}
